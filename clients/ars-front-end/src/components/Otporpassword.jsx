import "./otporpasswordpage-style.css";
import {FaMobileAlt} from 'react-icons/fa'
import { MdOutlinePassword } from "react-icons/md";
const Otporpassword = () => {
    return (
      <div>
        <div className="main-container">
          <div className="continue-with-otp-container">
            <a href='/'>Continue with OTP</a>
            <FaMobileAlt
              className="mobile-img"
              color="white"
              size="30px"
              style={{ marginTop: "0.5rem", paddingLeft: "10px",size:'25px' }}
            />
          </div>
          <div className="or-text">OR</div>
          <div className="continue-with-password-container">
            <a href='/'>Set a Password</a>
            <MdOutlinePassword
              className="password-img"
              color="white"
              size="30px"
              style={{ marginTop: "0.5rem", paddingLeft: "10px",size:'25px' }}
            />
          </div>
        </div>
      </div>
    );
}

export default Otporpassword;
