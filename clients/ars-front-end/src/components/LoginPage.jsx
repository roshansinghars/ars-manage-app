import { useState, useEffect } from "react";
import axios from "axios";

import "./homepage-style.css";
import "./loginpage-style.css";
import { BiChevronRightCircle } from "react-icons/bi";
const LoginPage = () => {
  const [mobileNumber, setMobileNumber] = useState(null);
  const handleChange = (event) => {
    setMobileNumber(event.target.value);
  };

  const getData=()=>{
axios
      .get({
        url: "http://127.0.0.1:8000/users/viewstudents/",
        // data: { phone_number: mobileNumber },
      })
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  // useEffect(() => {
  //   axios
  //     .get({
  //       url: "http://127.0.0.1:8000/users/viewstudents/",
  //       data: { phone_number: mobileNumber },
  //     })
  //     .then((response) => {
  //       console.log(response);
  //     })
  //     // .catch((err) => {
  //       console.log(err);
  //     });
  // }, []);

  return (
    <div>
      <div className="login-page-heading">
        <h1 className="primary-font">Create new account</h1>
      </div>
      <div className="login-page-sub-heading">
        <p className="primary-font">
          Already Registered?
          <a className="secondary-font" href="/">
            Login Here
          </a>
        </p>
      </div>
      <div className="login-page-text-content">
        <h2 className="secondary-font">Enter Your Mobile Number and Relax</h2>
      </div>
      <div className="input-select-container">
        <div className="std-code-dropdown">
          <select>
            <optgroup>
              <option value="+91">+91</option>
              <option value="+91">+91</option>
              <option value="+91">+91</option>
              <option value="+91">+91</option>
            </optgroup>
          </select>
        </div>
        <div className="phone-number-text-box">
          <input type="text" onChange={handleChange}></input>
        </div>
        <div
          className="login-page-go-btn"
          style={!!mobileNumber ? undefined : { visibility: "hidden" }}
          onClick={getData}
        >
          <BiChevronRightCircle color="white" size="3rem" />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
