import React from "react";
import "./homepage-style.css";
import homePageStudentIcon from "../assets/home-page-student-icon.png";
import homePageTeacherIcon from "../assets/homepage-teacher-icon.png";

const Homepage = () => {
  return (
    <div>
      <div>
        <header className="home-page-header">
          <h1 className="primary-font">Where should we take you?</h1>
        </header>
      </div>
      <div className="home-page-icons">
        <div className="student-icon-and-text">
          <a href="/user-login">
            <img src={homePageStudentIcon} alt=""></img>
          </a>
          <h1 className="primary-font"> Student </h1>
        </div>
        <div className="teacher-icon-and-text">
          <a href="/">
            <img src={homePageTeacherIcon} alt=""></img>
          </a>
          <h1 className="primary-font"> Teacher </h1>
        </div>
      </div>
      <div className="home-page-footer">
        <p className="secondary-font">
          Confused?&nbsp;``
          <a className="secondary-font" href="/">
            <b>Click me</b>
          </a>
        </p>
      </div>
    </div>
  );
};

export default Homepage;
