import "./homepage-style.css";
import "./otppage-style.css";
const OtpPage = () => {
  return (
    <div>
      <div className="main-container">
        <header className="otp-page-content">
          <h1 className="enter-otp-text">Enter the OTP</h1>
          <h3 className="otp-send-text">Enter the OTP sent to 7003147990</h3>
        </header>
        <div className="otp-box">
          <input type="text"></input>
        </div>
        {/* 
          Will have to provide the visibility property for resend and OTP incorrect test field.
           */}
        <div className="resend-otp-btn">
          <button style={{ visibility: "visible" }}>Resend OTP</button>
        </div>
        <div className="incorrect-otp-text">
          <h2 style={{ visibility: "visible" }}>Opps! Incorrect OTP</h2>
        </div>
      </div>
    </div>
  );
};

export default OtpPage;
