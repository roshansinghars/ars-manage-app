import "./App.css";
import Homepage from "./components/Homepage";
import LoginPage from "./components/LoginPage";
import OtpPage from "./components/OtpPage";
import Otporpassword from "./components/Otporpassword";
import { BrowserRouter as Router, Route,Switch } from "react-router-dom";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage />
        </Route>
        <Route exact path="/user-login">
          <LoginPage />
        </Route>
        <Route exact path="/verify-otp">
          <OtpPage />
        </Route>
        <Route exact path="/otp-password">
          <Otporpassword />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
