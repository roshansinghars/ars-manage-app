const express = require("express");
const route = require("./routes");
const app = express();

app.use(route);
app.listen(3000, () => {
  console.log("Running the server");
});
